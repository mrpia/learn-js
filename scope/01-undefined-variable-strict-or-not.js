// from https://www.sitepoint.com/5-typical-javascript-interview-exercises/

(function () {
    var a = b = 1;
})();

console.log(b);



(function () {
    'use strict';
    var c = d = 2;
})();

console.log(d);


(function () {
    'use strict';
    var e = global.f = 3;
})();

console.log(f);
