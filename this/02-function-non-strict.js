function f() {
    return this;
}

console.log(f() === global); // global in node, window in a browser

console.log(f());
