// An object can be passed as the first argument to call or apply and this will be bound to it.
var obj = {a: 'Custom'};

// This property is set on the global object
var a = 'Global';

function whatsThis() {
  console.log(this.a);  // The value of this is dependent on how the function is called
}

whatsThis();          // 'Global' in browser? // undefined in node???
whatsThis.call(obj);  // 'Custom'
whatsThis.apply(obj); // 'Custom'