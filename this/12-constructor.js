// this is the newly created object in a constructor invocation

/*
 * Constructors work like this:
 *
 * function MyConstructor(){
 *   // Actual function body code goes here.  
 *   // Create properties on |this| as
 *   // desired by assigning to them.  E.g.,
 *   this.fum = "nom";
 *   // et cetera...
 *
 *   // If the function has a return statement that
 *   // returns an object, that object will be the
 *   // result of the |new| expression.  Otherwise,
 *   // the result of the expression is the object
 *   // currently bound to |this|
 *   // (i.e., the common case most usually seen).
 * }
 */

function C() {
    this.a = 37;
  }
  
  var o = new C();
  console.log(o.a); // 37
  
  function C2() {
    this.a = 37;
    return {a: 38};
  }
  
  o = new C2();
  console.log(o.a); // 38
  