function Vehicle(type, wheelsCount) {
    this.type = type;
    this.wheelsCount = wheelsCount;
    return this;
}

// Function invocation instead of constructor invocation (no "new" keyword)
var car = Vehicle('Car', 4);
console.log(car.type); // => 'Car'  
console.log(car.wheelsCount); // => 4  
console.log(car === global); // => true

// protect yourself with instance check in constructor

function SaferVehicle(type, wheelsCount) {
    if (!(this instanceof SaferVehicle)) {
        throw new Error('oops, forgot the "new" keyword, did you?')
    }
    this.type = type;
    this.wheelsCount = wheelsCount;
    return this;
}

const safeCar = new SaferVehicle('Tank', 4);
console.log(safeCar.type);

const brokenCar = SaferVehicle('Car', 4); // throws Error
