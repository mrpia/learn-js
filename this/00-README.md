# The convoluted Rules of `this`

Examples from <https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/this>.

<https://dmitripavlutin.com/gentle-explanation-of-this-in-javascript/> might help too.