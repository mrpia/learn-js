// this on the object's prototype chain

// The same notion holds true for methods defined somewhere on the object's prototype chain.
// If the method is on an object's prototype chain, this refers to the object the method was called on, as if the method were on the object.

var o = {
    f: function () {
        return this.a + this.b;
    }
};
var p = Object.create(o);
p.a = 1;
p.b = 4;

console.log(p.f()); // 5

// In this example, the object assigned to the variable p doesn't have its own f property, it inherits it from its prototype.
// But it doesn't matter that the lookup for f eventually finds a member with that name on o; the lookup began as a reference to p.f,
// so this inside the function takes the value of the object referred to as p. That is, since f is called as a method of p, its this refers to p.
// This is an interesting feature of JavaScript's prototype inheritance.