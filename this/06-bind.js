// this is the first argument of .bind() when invoking a bound function

var numbers = {
    array: [3, 5, 10],
    getNumbers: function () {
        return this.array;
    }
};

// Create a bound function
var boundGetNumbers = numbers.getNumbers.bind(numbers);
console.log(boundGetNumbers()); // => [3, 5, 10]  

// Extract method from object
var simpleGetNumbers = numbers.getNumbers;
console.log(simpleGetNumbers()); // => undefined or throws an error in strict mode

// context binding is tight, binds works only once!

function getThis() {
    'use strict';
    return this;
}
var one = getThis.bind(1);
// Bound function invocation
console.log(one()); // => 1  
// Use bound function with .apply() and .call()
console.log(one.call(2)); // => 1  
console.log(one.apply(2)); // => 1  
// Bind again
console.log(one.bind(2)()); // => 1  

// Call the bound function as a constructor, but not recommended
console.log(new one()); // => Object