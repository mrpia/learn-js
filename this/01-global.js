// in node -> global object
console.log(this); // {}

this.b = 2;

console.log(this); // { b: 2 }
