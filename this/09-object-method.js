// When a function is called as a method of an object, its this is set to the object the method is called on.
// In the following example, when o.f() is invoked, inside the function this is bound to the o object.

var o = {
  prop: 37,
  f: function() {
    return this.prop;
  }
};

console.log(o.f()); // 37

// Note that this behavior is not at all affected by how or where the function was defined.
// In the previous example, we defined the function inline as the f member during the definition of o.
// However, we could have just as easily defined the function first and later attached it to o.f. Doing so results in the same behavior:

var o = {prop: 37};

function independent() {
  return this.prop;
}

o.f = independent;

console.log(o.f()); // 37

// This demonstrates that it matters only that the function was invoked from the f member of o.
// Similarly, the this binding is only affected by the most immediate member reference.

// In the following example, when we invoke the function, we call it as a method g of the object o.b.
// This time during execution, this inside the function will refer to o.b. The fact that the object is itself a member of o has no consequence;
// the most immediate reference is all that matters.

o.b = {g: independent, prop: 42};
console.log(o.b.g()); // 42



// this in methods refers to the object instance on which the method is called
// nothing too wild here I think

const incrementer = {
  value: 0,
  increment: function() {
      this.value++;
      return this.value;
  }
}

console.log(incrementer.increment());
console.log(incrementer.increment());
console.log(incrementer.increment());

const myStuff = Object.create({
  sayName: function () {
      return this.name;
  }
});

console.log(myStuff.sayName());
myStuff.name = 'joe';

console.log(myStuff.sayName());
