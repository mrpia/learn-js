class Planet {
    constructor(name) {
        this.name = name;
    }

    getName() {
        return this.name;
    }
}

const pluto = new Planet('pluto');

// method invocation. the context is pluto
console.log(pluto.name);