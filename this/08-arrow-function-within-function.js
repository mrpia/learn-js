// this is the enclosing context where the arrow function is defined

// Create obj with a method bar that returns a function that returns its this.

// The returned function is created as an arrow function,
// so its this is permanently bound to the this of its enclosing function.

// The value of bar can be set in the call,
// which in turn sets the value of the returned function.
var obj = {
    bar: function () {
        var x = (() => this);
        return x;
    }
};

// Call bar as a method of obj, setting its this to obj
// Assign a reference to the returned function to fn
var fn = obj.bar();

// Call fn without setting this, would normally default
// to the global object or undefined in strict mode
console.log(fn() === obj); // true

// But caution if you reference the method of obj without calling it
var fn2 = obj.bar;
// Then calling the arrow function this is equals to window because it follows the this from bar.
console.log(fn2()() === global); // true

// In the above, the function(call it anonymous function A) assigned to obj.bar
// returns another function (call it anonymous function B) that is created as an arrow function.
// As a result, function B's this is permanently set to the this of obj.bar (function A) when called.
// When the returned function(function B) is called, its this will always be what it was set to initially.
// In the above code example, function B's this is set to function A's this which is obj,
// so it remains set to obj even when called in a manner that would normally set its this to undefined
// or the global object (or any other method as in the previous example in the global execution context).
