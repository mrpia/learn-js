const animal = {
    name: 'pumba',
    sayName: function() {
        return `my name is ${this.name}`;
    }
};

const detachedSayName = animal.sayName;

console.log(animal.sayName()); // pumba

console.log(detachedSayName()); // undefined

// bind to the rescue

const boundSayName = animal.sayName.bind(animal);
console.log(boundSayName());