// this is the enclosing context where the arrow function is defined

// In arrow functions, this retains the value of the enclosing lexical context's this
// In global code, it will be set to the global object:

var globalObject = this;
var foo = (() => this);
console.log(foo() === globalObject); // true

// Note: if this arg is passed to call, bind, or apply on invocation of an arrow function it will be ignored.
// You can still prepend arguments to the call, but the first argument (thisArg) should be set to null.

// Call as a method of an object
var obj = {func: foo};
console.log(obj.func() === globalObject); // true

// Attempt to set this using call
console.log(foo.call(obj) === globalObject); // true

// Attempt to set this using bind
foo = foo.bind(obj);
console.log(foo() === globalObject); // true