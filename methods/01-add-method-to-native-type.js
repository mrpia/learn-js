// from https://www.sitepoint.com/5-typical-javascript-interview-exercises/

String.prototype.repeatify = String.prototype.repeatify || function(times) {
    let result = '';
    for (let i = 0; i < times; i++) {
        result += this;
    }
    return result;
};

'blah...'.repeatify(3);