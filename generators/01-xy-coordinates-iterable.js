// see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Iteration_protocols

function gridCoordinates(fromX, toX, fromY, toY) {

    return {
        [Symbol.iterator]: iterator
    };

    function iterator() {
        let x = fromX;
        let y = fromY;
        
        return {
            next: next
        };
    
        function next() {
            if (x > toX) {
                x = fromX;
                y++;
            }
            if (y > toY) {
                return { done: true };
            }
            const result = { x: x, y: y };
            x++;
            return { value: result };
        }
    }
}

for (let coordinate of gridCoordinates(0, 2, 0, 2)) {
    console.log(coordinate);
}
