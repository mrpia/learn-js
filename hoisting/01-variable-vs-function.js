// from https://www.sitepoint.com/5-typical-javascript-interview-exercises/

(function() {
    console.log(a);
    foo();

    var a = 1;

    function foo() {
        console.log('foo');
    }
})();
